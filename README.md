## Version française [ici](README_fr.md)
# Scodoc Notes

Scodoc Notes is an Android app allowing students in french IUT to see their grades on their mobiles.

⚠️: The app is **UNOFFICIAL** and is not endorsed by any university nor by the [Scodoc-Notes Platform](https://github.com/SebL68/Scodoc_Notes). It is a project made by a student, during his free time, to improve the usability of the platform

## Features
Features include: 
- Connect through the "ENT" (CAS) and **stay logged in** !
- See your grades with a pleasing interface on the "Relevé" page
- Take a look at your latest grades on the "Nouvelles évaluations" page
- Recieve notifications when you get a new grade

## Currently supported IUTs :
- IUT d'Amiens (not tested)
- IUT d'Annecy (not tested)
- IUT de Beziers (not tested)
- IUT de Brest (not tested)
- IUT de Chartres
- IUT de Lille (not tested)
- IUT de Mulhouse (not tested)
- IUT de Nantes (not tested)
- IUT d'Orléans
- IUT d'Orsay (not tested)
- IUT de Saint-Nazaire (not tested)
- IUT de Vélizy (not tested)
- IUT de Villetaneuse / Paris 13 (not tested)


👋 : If you want your IUT to be supported, contact me at julien@julienrosse.fr or open an issue here. I need students from missing IUTs to test if the integration works well as I do not have credentials to every IUTs.

## How does it work ?
The app logs in to the CAS (Central Authentication Service) and retrieves the grades from the [Scodoc-Notes Platform](https://github.com/SebL68/Scodoc_Notes) instance of the IUT

## Credits
App icon made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)

## License
Read [COPYING](COPYING) for more information
```
Scodoc Notes - Allow IUT students to check their grades
Copyright (C) 2023 Julien ROSSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
