# Scodoc Notes

Scodoc Notes est une application Android qui permet aux étudiants des IUT de France de regarder leurs notes sur leurs mobiles.

⚠️: Cette application **N'EST PAS OFFICIELLE** et n'est reconnue par aucune université ni par la [plateforme Scodoc-Notes](https://github.com/SebL68/Scodoc_Notes). C'est un projet réalisé par un étudiant, pendant son temps libre, dans le but d'améliorer l'expérience utilisateur de la plateforme sur mobile.

## Fonctionnalités :
Voici les fonctionnalités de l'application :
- Se connecter à l'ENT (CAS) et **rester connecté(e)** !
- Regarder ses notes avec une interface agréable sur la page "Relevé"
- Jeter un coup d'oeuil à ses dernières notes sur la page "Nouvelles évaluations"
- Recevoir des notifications lors de la réception d'une nouvelle note

## IUT actuellement supportés :
- IUT d'Amiens (non testé)
- IUT d'Annecy (non testé)
- IUT de Beziers (non testé)
- IUT de Brest (non testé)
- IUT de Chartres
- IUT de Lille (non testé)
- IUT de Mulhouse (non testé)
- IUT de Nantes (non testé)
- IUT d'Orléans
- IUT d'Orsay (non testé)
- IUT de Saint-Nazaire (non testé)
- IUT de Vélizy (non testé)
- IUT de Villetaneuse / Paris 13 (non testé)

👋: Si vous voulez que votre IUT soit ajouté, contactez moi à l'adresse suivante : julien@julienrosse.fr or ouvrez une "issue" ici. 

Si votre IUT est marqué, j'apprécierais que vous me contactiez pour me dire si l'intégration fonctionne bien.

## Comment ça marche ?
L'application se connecte au CAS (Central Authentication Service) de l'université et récupère les notes depuis l'instance de la [plateforme Scodoc-Notes](https://github.com/SebL68/Scodoc_Notes) de l'IUT

## Crédits
Icone de l'application réalisée par [Freepik](https://www.flaticon.com/authors/freepik) de [Flaticon](https://www.flaticon.com/)

## Licence
Lire [COPYING](COPYING) pour plus d'informations
```
Scodoc Notes - Allow IUT students to check their grades
Copyright (C) 2023 Julien ROSSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```