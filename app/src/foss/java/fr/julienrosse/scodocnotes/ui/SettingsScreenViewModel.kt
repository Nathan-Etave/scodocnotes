/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.julienrosse.scodocnotes.Graph
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SettingsScreenViewModel() : ViewModel() {
    val uiState = mutableStateOf(Graph.userSettings)

    private fun save(){
        Graph.userSettings = uiState.value
        viewModelScope.launch(Dispatchers.IO) {
            Graph.settingsStore.saveSettings(Graph.userSettings)
            println("Saved settings")
        }
    }
    fun toggleNotificationEnabled() {
        uiState.value = uiState.value.copy(notificationEnabled = !uiState.value.notificationEnabled)
        if (uiState.value.notificationEnabled) {
            Graph.initNewNotificationWorker()
        } else {
            Graph.stopNewNotificationWorker()
        }
        save()
    }

    fun setNotificationRefreshInterval(interval: String) {
        val intervalInt = interval.toIntOrNull()
        if (intervalInt != null && intervalInt > 0) {
            uiState.value = uiState.value.copy(notificationRefreshInterval = intervalInt)
            save()
        }
    }
}
