/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Book
import androidx.compose.material.icons.filled.Inbox
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.core.app.ActivityCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mikepenz.aboutlibraries.ui.compose.LibrariesContainer
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.ui.theme.ScodocNotesTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.POST_NOTIFICATIONS),
                1
            )
        }
        setContent {
            ScodocNotesTheme {
                Column(
                    Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                ) {
                    val navController = rememberNavController()
                    val startDestination =
                        if (Graph.userRepository.hasStoredCreds()) "releve" else "login"
                    var showNavBar by remember { mutableStateOf(startDestination != "login") }
                    NavHost(
                        navController = navController,
                        startDestination = startDestination,
                        modifier = Modifier.weight(1f)
                    ) {
                        composable("login") {
                            LoginScreen(LoginViewModel(onLoginSuccess = {
                                showNavBar = true
                                navController.navigate("releve")
                            }, onLoginFailure = {}))
                        }
                        composable("releve") {
                            ReleveScreen()
                        }
                        composable("evaluations") {
                            EvaluationsScreen()
                        }
                        composable("settings") {
                            SettingsScreen(navController = navController)
                        }
                        composable("licenses") {
                            LibrariesContainer(Modifier.fillMaxSize())
                        }
                    }
                    var selectedIndex by remember { mutableStateOf(0) }
                    if (showNavBar) {
                        NavigationBar {
                            NavigationBarItem(
                                selected = (selectedIndex == 0),
                                onClick = {
                                    selectedIndex = 0
                                    navController.navigate("releve")
                                },
                                icon = { Icon(Icons.Default.Book, contentDescription = "Relevé") },
                                label = { Text("Relevé") }
                            )
                            NavigationBarItem(
                                selected = (selectedIndex == 1),
                                onClick = {
                                    selectedIndex = 1
                                    navController.navigate("evaluations")
                                },
                                icon = {
                                    Icon(
                                        Icons.Default.Inbox,
                                        contentDescription = "Nouvelles notes"
                                    )
                                }, label = { Text("Nouvelles notes") }
                            )
                            NavigationBarItem(
                                selected = (selectedIndex == 2),
                                onClick = {
                                    selectedIndex = 2
                                    navController.navigate("settings")
                                },
                                icon = {
                                    Icon(
                                        Icons.Default.Settings,
                                        contentDescription = "Paramètres"
                                    )
                                }, label = { Text("Paramètres") }
                            )
                        }
                    }
                }

            }
        }
    }

}
