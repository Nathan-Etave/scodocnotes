/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class ScodocFetcherTest {
    private lateinit var server: MockWebServer
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var fetcher: ScodocFetcher
    @Before
    fun setUp() {
        server = MockWebServer()
        okHttpClient = OkHttpClient()
        fetcher = ScodocFetcher(okHttpClient)
        server.start()
    }
    @Test
    fun getDataPremiereConnexion() {
        server.enqueue(MockResponse().setBody(fetcher.testDPC))
        val url = server.url("/").toString()
        val result = fetcher.getDataPremiereConnexion(url)
        assertEquals(fetcher.testDPC, result)

        server.enqueue(MockResponse().setBody("{\"redirect\":\"\\/services\\/doAuth.php\"}"))
        try {
            fetcher.getDataPremiereConnexion(url)
            fail("Should have thrown an exception")
        } catch (e: Exception) {
            assertTrue(e is AuthException)
        }
    }
}