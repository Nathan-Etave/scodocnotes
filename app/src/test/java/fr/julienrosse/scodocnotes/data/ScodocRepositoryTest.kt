/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import com.google.gson.GsonBuilder
import fr.julienrosse.scodocnotes.data.model.Note
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*

import org.junit.Test

class ScodocRepositoryTest {
    @Test
    fun updateDataPremiereConnexion() {
        val scodocFetcher = mockk<ScodocFetcher>()
        every { scodocFetcher.getDataPremiereConnexion(any()) } returns "{\"relevé\":{\"ressources\":{\"R2.01\":{\"evaluations\":[{\"coef\":\"01.00\",\"date\":\"2023-03-06\",\"description\":\"Devoir 1\",\"evaluation_type\":0,\"heure_debut\":\"09:30\",\"heure_fin\":\"10:30\",\"id\":2081,\"note\":{\"max\":\"19.50\",\"min\":\"02.50\",\"moy\":\"14.16\",\"value\":\"19.50\"},\"poids\":{\"UE2.1\":1,\"UE2.2\":1,\"UE2.3\":0,\"UE2.4\":0,\"UE2.5\":0,\"UE2.6\":0}}],\"id\":11114,\"titre\":\"Développement orienté objets\"},\"R2.02\":{\"evaluations\":[],\"id\":11121,\"titre\":\"Développement d'application avec IHM\"}},\"saes\":{\"S2.01\":{\"evaluations\":[],\"id\":11116,\"titre\":\"Développement d'une application\"},\"S2.02\":{\"evaluations\":[],\"id\":11133,\"titre\":\"Exploration algorithmique d'un problème\"}},\"ues\":{\"UE2.1\":{\"bonus\":\"00.00\",\"capitalise\":null,\"color\":\"#b80004\",\"competence\":null,\"ECTS\":{\"acquis\":0,\"total\":5},\"id\":1151,\"malus\":\"00.00\",\"moyenne\":{\"groupes\":{},\"max\":\"~\",\"min\":\"~\",\"moy\":\"~\",\"rang\":\"\",\"total\":0,\"value\":\"~\"},\"numero\":6,\"ressources\":{\"R2.01\":{\"coef\":21,\"id\":11114,\"moyenne\":\"~\"},\"R2.02\":{\"coef\":21,\"id\":11121,\"moyenne\":\"~\"}},\"saes\":{\"S2.01\":{\"coef\":38,\"id\":11116,\"moyenne\":\"~\"}},\"titre\":\"COMPÉTENCE 1 : Réaliser un développement d'applications\"},\"UE2.2\":{\"bonus\":\"00.00\",\"capitalise\":null,\"color\":\"#f97b3d\",\"competence\":null,\"ECTS\":{\"acquis\":0,\"total\":5},\"id\":1152,\"malus\":\"00.00\",\"moyenne\":{\"groupes\":{},\"max\":\"20.00\",\"min\":\"03.50\",\"moy\":\"14.11\",\"rang\":\"\",\"total\":0,\"value\":\"14.33\"},\"numero\":7,\"ressources\":{\"R2.01\":{\"coef\":15,\"id\":11114,\"moyenne\":\"~\"}},\"saes\":{\"S2.02\":{\"coef\":38,\"id\":11133,\"moyenne\":\"~\"}},\"titre\":\"COMPÉTENCE 2 : Optimiser des applications informatiques\",\"type\":0}},\"ues_capitalisees\":{},\"version\":\"0\"},\"semestres\":[{\"annee_scolaire\":\"2022\\/2023\",\"formsemestre_id\":522,\"semestre_id\":1,\"titre\":\"BUT\"},{\"annee_scolaire\":\"2022\\/2023\",\"formsemestre_id\":557,\"semestre_id\":2,\"titre\":\"BUT\"}]}"
        val gson = GsonBuilder()
            .registerTypeAdapter(Note::class.java, NoteDeserializer())
            .create()
        val repository = ScodocRepository(scodocFetcher, "http://localhost", gson)
        repository.updateDataPremiereConnexion()
        assertNotNull(repository.dataPremiereConnexion)
        assertEquals(repository.dataPremiereConnexion!!.releve.ressources.size, 2)
    }
}