/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import com.google.gson.Gson
import com.google.gson.JsonObject
import fr.julienrosse.scodocnotes.data.model.Note
import org.junit.Assert.assertEquals
import org.junit.Test


internal class NoteDeserializerTest {

    @Test
    fun deserialize() {
        val note = NoteDeserializer().deserialize(null, null, null)
        assertEquals(null, note.value)
        assertEquals(null, note.min)
        assertEquals(null, note.max)
        assertEquals(null, note.moy)
        val gson = Gson()

        val json2 = "{\"value\":\"19.0\",\"min\":\"14.5\",\"max\":\"19.0\",\"moy\":\"12.25\"}"
        val note2 = NoteDeserializer().deserialize(gson.fromJson(json2, JsonObject::class.java), Note::class.java, null)
        assertEquals(19.0f, note2.value)
        assertEquals(14.5f, note2.min)
        assertEquals(19.0f, note2.max)
        assertEquals(12.25f, note2.moy)

        val json3 = "{\"value\":\"~\",\"min\":\"~\",\"max\":\"~\",\"moy\":\"~\"}"
        val note3 = NoteDeserializer().deserialize(gson.fromJson(json3, JsonObject::class.java), Note::class.java, null)
        assertEquals(null, note3.value)
        assertEquals(null, note3.min)
        assertEquals(null, note3.max)
        assertEquals(null, note3.moy)
    }
}