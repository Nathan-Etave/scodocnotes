/*
 *     Scodoc Notes - Allow IUT students to check their grades
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes

import android.content.Context
import com.posthog.android.PostHog




fun flavorSpecificInit(context: Context) {
    if (Graph.userSettings.telemetryEnabled == true){
        enableTelemetry(context)
    }
}
private const val POSTHOG_API_KEY = "phc_OXNJVMTMiesvcXT8HkMg2efZwllIyN4Svev4G6u2K1Q"
private const val POSTHOG_HOST = "https://eu.posthog.com"

fun enableTelemetry(context: Context) {
    // Set the initialized instance as a globally accessible instance
    try {
        val posthog = PostHog.Builder(context, POSTHOG_API_KEY, POSTHOG_HOST)
            .captureApplicationLifecycleEvents() // Record certain application events automatically!
            .collectDeviceId(false) // Don't send the device ID!
            .build()
        PostHog.setSingletonInstance(posthog)
    } catch (e: IllegalStateException) {
        // There can only be one instance of PostHog
        // Use the existing instance instead
    }
}

fun disableTelemetry(context: Context) {
    PostHog.with(context).optOut(true)
}