/*
 *     Scodoc Notes - Allow IUT students to check their grades
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.AlertDialogDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TelemetryConsentDialog(onUserAnswer: (Boolean) -> Unit) {
    AlertDialog(onDismissRequest = {onUserAnswer(false)}){
        Surface (
            Modifier
                .wrapContentHeight()
                .wrapContentWidth(),
            shape = AlertDialogDefaults.shape,
            tonalElevation = AlertDialogDefaults.TonalElevation
        ){
            Column(Modifier.padding(16.dp)) {
                Text(text = "Autorisation de collecte de données")
                Spacer(modifier = Modifier.padding(24.dp))
                Text(text = "Scodoc Notes collecte des données anonymes sur l'utilisation de l'application afin d'améliorer l'expérience utilisateur.")
                Text(text = "Ces données contiennent des informations sur les fonctionnalités utilisées et les erreurs rencontrées, ainsi que l'université de l'utilisateur.")
                Spacer(modifier = Modifier.padding(24.dp))
                Row {
                    TextButton(onClick = { onUserAnswer(false) }) {
                      Text(text = "Refuser")
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    TextButton(onClick = { onUserAnswer(true) }) {
                      Text(text = "Accepter")
                    }
                }
            }
        }
    }
}
