/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui


import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Balance
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.Logout
import androidx.compose.material.icons.filled.OpenInBrowser
import androidx.compose.material.icons.filled.Security
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.disableTelemetry
import fr.julienrosse.scodocnotes.enableTelemetry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun SettingsScreen(
    settingsScreenViewModel: SettingsScreenViewModel = viewModel(),
    navController: NavController
) {
    val state = settingsScreenViewModel.uiState.value
    val context = LocalContext.current
    if (settingsScreenViewModel.telemetryStateChange.value){
        settingsScreenViewModel.telemetryStateChange.value = false
        if (Graph.userSettings.telemetryEnabled == true) {
            enableTelemetry(context)
        } else {
            disableTelemetry(context)
        }
    }
    val uriHandler = LocalUriHandler.current
    LazyColumn(Modifier.padding(16.dp), verticalArrangement = Arrangement.spacedBy(16.dp)){
        item {
            Row{
                Text(text = "Activer les notification de nouvelles notes", Modifier.weight(.5f), color = MaterialTheme.colorScheme.onBackground)
                Spacer(modifier = Modifier.weight(.1f))
                Switch(
                    checked = state.notificationEnabled,
                    onCheckedChange = { settingsScreenViewModel.toggleNotificationEnabled() },
                    modifier = Modifier.weight(.4f)
                )
            }
        }
        item {
            Row{
                Text(text = "Intervalle de rafraichissement des notifications", Modifier.weight(.5f), color = MaterialTheme.colorScheme.onBackground)
                Spacer(modifier = Modifier.weight(.1f))
                TextField(
                    value = state.notificationRefreshInterval.toString(),
                    onValueChange = {settingsScreenViewModel.setNotificationRefreshInterval(it)},
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    suffix = { Text(text = "h") },
                    maxLines = 1,
                    modifier = Modifier.weight(.4f)
                )
            }
        }
        item {
            Row{
                Text(text = "Activer la télémétrie", Modifier.weight(.5f), color = MaterialTheme.colorScheme.onBackground)
                Spacer(modifier = Modifier.weight(.1f))
                Switch(
                    checked = state.telemetryEnabled == true,
                    onCheckedChange = { settingsScreenViewModel.toggleTelemetryEnabled() },
                    modifier = Modifier.weight(.4f)
                )
            }
        }
        item{
            ListItem(
                leadingContent = { Icon(Icons.Default.Logout, contentDescription = "Icone de déconnexion") },
                headlineContent = { Text(text = "Se déconnecter") },
                trailingContent = { Icon(Icons.Default.ChevronRight, contentDescription = "Fleche droite") },
                modifier =  Modifier.clickable {
                    Toast.makeText(
                        context,
                        "Veillez redémarrer l'application",
                        Toast.LENGTH_SHORT
                    ).show()
                    CoroutineScope(Dispatchers.IO).launch {
                        Graph.scodocRepository.logout()
                        Graph.userRepository.logout()
                        context.packageManager.getLaunchIntentForPackage(context.packageName)?.let {
                            context.startActivity(it)
                            Runtime.getRuntime().exit(0)
                        }
                    }
                }
            )
        }
        item {
            Divider()
            ListItem(
                leadingContent = { Icon(Icons.Default.Balance, contentDescription = "Icone de justice") },
                headlineContent = { Text(text = "Licence : GNU GPL v3") },
                trailingContent = { Icon(Icons.Default.OpenInBrowser, contentDescription = "Icone de lien") },
                modifier = Modifier.clickable { uriHandler.openUri("https://www.gnu.org/licenses/gpl-3.0.fr.html") }
            )
        }
        item {
            ListItem(
                leadingContent = {
                    Icon(
                        Icons.Default.Balance,
                        contentDescription = "Icone de justice"
                    )
                },
                headlineContent = { Text(text = "Licences des bibliothèques") },
                trailingContent = {
                    Icon(
                        Icons.Default.ChevronRight,
                        contentDescription = "Ouvrir"
                    )
                },
                modifier = Modifier.clickable {
                    navController.navigate("licenses")
                }
            )
        }

        item {
            ListItem(
                leadingContent = { Icon(Icons.Default.Security, contentDescription = "") },
                headlineContent = { Text(text = "Politique de confidentialité") },
                trailingContent = { Icon(Icons.Default.OpenInBrowser, contentDescription = "Icone de lien") },
                modifier = Modifier.clickable { uriHandler.openUri("https://scodocnotes.julienrosse.fr/privacy") }
            )
        }
    }
}

