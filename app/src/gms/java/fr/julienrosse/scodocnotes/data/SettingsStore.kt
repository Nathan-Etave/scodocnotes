/*
 *     Scodoc Notes - Allow IUT students to check their grades
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class SettingsStore(context: Context) {
    private val Context.settingsDataStore by preferencesDataStore("settings")
    private val dataStore = context.settingsDataStore

    suspend fun getSettings(): UserSettings {
        return dataStore.data.map { preferences ->
            UserSettings(
                notificationEnabled = preferences[DataStoreKeys.NOTIFICATION_ENABLED] ?: true,
                notificationRefreshInterval = preferences[DataStoreKeys.NOTIFICATION_REFRESH_INTERVAL] ?: 1,
                telemetryEnabled = preferences[DataStoreKeys.TELEMETRY_ENABLED]
            )
        }.first()
    }

    suspend fun saveSettings(settings: UserSettings) {
        dataStore.edit { preferences ->
            preferences[DataStoreKeys.NOTIFICATION_ENABLED] = settings.notificationEnabled
            preferences[DataStoreKeys.NOTIFICATION_REFRESH_INTERVAL] = settings.notificationRefreshInterval
            preferences[DataStoreKeys.TELEMETRY_ENABLED] = settings.telemetryEnabled?: false
        }
    }
}