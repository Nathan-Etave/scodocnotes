package fr.julienrosse.scodocnotes

import android.app.Application
import android.content.Context
import org.acra.config.dialog
import org.acra.config.mailSender
import org.acra.data.StringFormat
import org.acra.ktx.initAcra

class ScodocNotesApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Graph.provide(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        initAcra {
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON

            mailSender {
                reportAsFile = true
                mailTo = "julien@julienrosse.fr"
                subject = "Scodoc Notes Crash Report"
            }
            dialog {
                positiveButtonText = "Envoyer"
                negativeButtonText = "Ne pas envoyer"
                commentPrompt = "Commentaire (optionnel)"
                text = "Une erreur est survenue. Voulez-vous envoyer un rapport de bug ?"
                title = "Crash de Scodoc Notes"
            }
        }
    }
}