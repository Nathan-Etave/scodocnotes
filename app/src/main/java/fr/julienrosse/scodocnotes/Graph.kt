/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes

import android.content.Context
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.julienrosse.scodocnotes.data.CASFetcher
import fr.julienrosse.scodocnotes.data.EvaluationRepository
import fr.julienrosse.scodocnotes.data.KnownEvaluationsStore
import fr.julienrosse.scodocnotes.data.NewNoteNotificationWorker
import fr.julienrosse.scodocnotes.data.NoteDeserializer
import fr.julienrosse.scodocnotes.data.NotificationRepository
import fr.julienrosse.scodocnotes.data.RangDeserializer
import fr.julienrosse.scodocnotes.data.ScodocFetcher
import fr.julienrosse.scodocnotes.data.ScodocRepository
import fr.julienrosse.scodocnotes.data.SettingsStore
import fr.julienrosse.scodocnotes.data.StoredCredsRepository
import fr.julienrosse.scodocnotes.data.UserRepository
import fr.julienrosse.scodocnotes.data.UserSettings
import fr.julienrosse.scodocnotes.data.model.Note
import fr.julienrosse.scodocnotes.data.model.SemestreRang
import fr.julienrosse.scodocnotes.data.persistencookies.PersistentCookieJar
import fr.julienrosse.scodocnotes.data.persistencookies.cache.SetCookieCache
import fr.julienrosse.scodocnotes.data.persistencookies.persistence.SharedPrefsCookiePersistor
import fr.julienrosse.scodocnotes.data.universities.IUTAmiens
import fr.julienrosse.scodocnotes.data.universities.IUTAnnecy
import fr.julienrosse.scodocnotes.data.universities.IUTBeziers
import fr.julienrosse.scodocnotes.data.universities.IUTBrest
import fr.julienrosse.scodocnotes.data.universities.IUTChartres
import fr.julienrosse.scodocnotes.data.universities.IUTLille
import fr.julienrosse.scodocnotes.data.universities.IUTMulhouse
import fr.julienrosse.scodocnotes.data.universities.IUTNantes
import fr.julienrosse.scodocnotes.data.universities.IUTOrleans
import fr.julienrosse.scodocnotes.data.universities.IUTOrsay
import fr.julienrosse.scodocnotes.data.universities.IUTSaintNazaire
import fr.julienrosse.scodocnotes.data.universities.IUTVelizy
import fr.julienrosse.scodocnotes.data.universities.IUTVilletaneuse
import fr.julienrosse.scodocnotes.data.universities.TestUniversity
import fr.julienrosse.scodocnotes.data.universities.University
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.tls.HandshakeCertificates
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit

object Graph {
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var okHttpClientNoRedirect: OkHttpClient
    private lateinit var storedCredsRepository: StoredCredsRepository
    private lateinit var workManager: WorkManager
    private lateinit var knownEvaluationsStore: KnownEvaluationsStore
    lateinit var gson: Gson
        private set
    private lateinit var notificationRepository: NotificationRepository
    lateinit var university: University
    val universities = listOf(
        IUTAmiens(),
        IUTAnnecy(),
        IUTBeziers(),
        IUTBrest(),
        IUTChartres(),
        IUTLille(),
        IUTMulhouse(),
        IUTNantes(),
        IUTOrleans(),
        IUTOrsay(),
        IUTSaintNazaire(),
        IUTVelizy(),
        IUTVilletaneuse(),
        TestUniversity()
    )
    val evaluationRepository by lazy {
        EvaluationRepository(
            scodocRepository,
            knownEvaluationsStore
        )
    }
    val userRepository by lazy {
        UserRepository(
            casFetcher,
            storedCredsRepository
        )
    }
    private val casFetcher by lazy {
        CASFetcher(
            okHttpClient,
            okHttpClientNoRedirect
        )
    }
    val scodocRepository by lazy {
        ScodocRepository(
            scodocFetcher,
            university.scodocBaseUrl
        )
    }
    private val scodocFetcher by lazy {
        ScodocFetcher(
            okHttpClient
        )
    }
    lateinit var userSettings: UserSettings
    lateinit var settingsStore: SettingsStore
        private set

    fun initNewNotificationWorker(){
        val newNotesUpdateConstraints = androidx.work.Constraints.Builder() //TODO: Add user config
            .setRequiredNetworkType(androidx.work.NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()
        val newNotesUpdateRequest =
            PeriodicWorkRequestBuilder<NewNoteNotificationWorker>(userSettings.notificationRefreshInterval.toLong(), TimeUnit.HOURS)
                .setConstraints(newNotesUpdateConstraints)
                .setInitialDelay(userSettings.notificationRefreshInterval.toLong(), TimeUnit.HOURS)
                .build()
        workManager.enqueueUniquePeriodicWork(
            "new_notes",
            ExistingPeriodicWorkPolicy.UPDATE,
            newNotesUpdateRequest
        )
        println("NewNoteNotificationWorker enqueued")
    }

    fun stopNewNotificationWorker(){
        workManager.cancelUniqueWork("new_notes")
        println("NewNoteNotificationWorker cancelled")
    }

    fun provide(context: Context) {
        storedCredsRepository = StoredCredsRepository(context)
        workManager = try {
            WorkManager.getInstance(context)
        } catch (e: IllegalStateException){
            WorkManager.initialize(context, androidx.work.Configuration.Builder().build())
            WorkManager.getInstance(context)
        }
        knownEvaluationsStore = KnownEvaluationsStore(context)
        notificationRepository = NotificationRepository(context)
        university = storedCredsRepository.getCreds()?.third ?: universities[0]

        settingsStore = SettingsStore(context)
        runBlocking {
            userSettings = settingsStore.getSettings()
        }

        if (userSettings.notificationEnabled){
            initNewNotificationWorker()
        }
        else{
            stopNewNotificationWorker()
        }

        val cookieJar = PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
        val geantCa = CertificateFactory.getInstance("X.509")
            .generateCertificate(context.assets.open("GEANT-OV-RSA-CA-4.crt")) as X509Certificate
        val certificates: HandshakeCertificates = HandshakeCertificates.Builder()
            .addPlatformTrustedCertificates()
            .addTrustedCertificate(geantCa)
            .build()
        okHttpClient = OkHttpClient.Builder()
            .sslSocketFactory(certificates.sslSocketFactory(), certificates.trustManager)
            .cookieJar(cookieJar)
            .addNetworkInterceptor(LoggingInterceptor())
            .addNetworkInterceptor(UserAgentAdderInterceptor())
            .build()
        okHttpClientNoRedirect = OkHttpClient.Builder()
            .sslSocketFactory(certificates.sslSocketFactory(), certificates.trustManager)
            .cookieJar(cookieJar)
            .addNetworkInterceptor(LoggingInterceptor())
            .addNetworkInterceptor(UserAgentAdderInterceptor())
            .followRedirects(false)
            .build()
        gson = GsonBuilder()
            .registerTypeAdapter(Note::class.java, NoteDeserializer())
            .registerTypeAdapter(SemestreRang::class.java, RangDeserializer())
            .create()
        flavorSpecificInit(context)
    }
}