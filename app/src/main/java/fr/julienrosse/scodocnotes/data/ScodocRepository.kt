/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import com.google.gson.Gson
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.model.DataPremiereConnexion
import fr.julienrosse.scodocnotes.data.model.Releve
import java.io.IOException

class ScodocRepository(
    private val scodocFetcher: ScodocFetcher,
    private val baseUrl: String,
    private val gson: Gson = Graph.gson,
) {
    private fun getDataPremiereConnexion(@Suppress("SameParameterValue") isRetry: Boolean = false): DataPremiereConnexion {
        return try {
            val response =
                if (baseUrl != "") scodocFetcher.getDataPremiereConnexion(baseUrl) else scodocFetcher.testDPC

            val dpc = gson.fromJson(response, DataPremiereConnexion::class.java)
            if (dpc.erreur != null) throw DPCError(dpc.erreur)
            Releve.populateMissing(dpc.releve)
            dpc // return

        } catch (_: AuthException) {
            Graph.userRepository.login()
            if (!isRetry) {
                getDataPremiereConnexion(true)
            } else {
                throw AuthException()
            }
        }
    }

    fun logout() {
        if (baseUrl == "") return // Test University
        scodocFetcher.logout(baseUrl)
    }
    var dataPremiereConnexion: DataPremiereConnexion? = null
        private set

    /**
     * Update the [dataPremiereConnexion] property with the latest data from the server.
     * @throws AuthException if the user is not logged in.
     * @throws IOException if an error occurs while fetching the data.
     * @throws DPCError if the server returns an error (like the Releve not being available).
     */
    fun updateDataPremiereConnexion() {
        dataPremiereConnexion = getDataPremiereConnexion()
    }
}