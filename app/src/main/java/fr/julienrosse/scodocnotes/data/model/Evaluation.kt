/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data.model

import java.util.Date

data class Evaluation(
    val id: Int,
    val coef: Float,
    val date: Date,
    val description: String?,
    val note: Note,
    val poids: Map<String, Int>,
    var ressource: Ressource? = null
) {
    companion object {
        /**
         * Calcule une variation de moyenne de la ressource si l'évaluation est absente
         * @param evaluation l'évaluation à retirer
         */
        fun varRessource(evaluation: Evaluation): Float? {
            val moyRes = Releve.calculMoyenne(evaluation.ressource!!)
            val moySansEval =
                Releve.calculMoyenne(evaluation.ressource!!.evaluations.filter { it.id != evaluation.id })
            return if (moyRes != null && moySansEval != null) {
                moyRes - moySansEval
            } else {
                null
            }
        }
    }
}