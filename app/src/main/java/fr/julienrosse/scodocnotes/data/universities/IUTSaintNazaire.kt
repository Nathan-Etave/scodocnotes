/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data.universities

/**
 * IUT de Saint-Nazaire
 * Fonctionnement non vérifié
 */
data class IUTSaintNazaire(
    override val nom: String = "IUT de Saint-Nazaire",
    override val casBaseUrl: String = "https://cas6.univ-nantes.fr/esup-cas-server",
    override val scodocBaseUrl: String = "https://notes.iut-sn.univ-nantes.fr"

) : University()