/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.jsoup.Jsoup
import java.net.URLEncoder

class CASFetcher(
    private val okHttpClient: OkHttpClient,
    private val okHttpClientNoRedirect: OkHttpClient
) {

    /**
     * If authenticated, returns the URL to redirect to, including the ticket
     * @throws NoTicketException if no ticket is found, with the execution key to use to login
     */
    fun getLogin(casUrl: String, redirectUrl: String): String {
        val request = Request.Builder()
            .url("$casUrl/login?service=$redirectUrl")
            .build()
        val response = okHttpClientNoRedirect.newCall(request).execute()

        if (response.isRedirect && response.header("Location") != null) return response.header("Location")!!
        val doc = Jsoup.parse(
            response.body?.string() ?: throw RuntimeException("Failed to parse CAS response")
        )
        val execElms = doc.getElementsByAttributeValue("name", "execution")
        if (execElms.size == 0) throw RuntimeException()
        throw NoTicketException(execElms[0].`val`())
    }

    /**
     * Follows provided redirect URL, which should contain a ticket
     * Session cookie should be added to the [okHttpClient] CookieJar
     *
     * TODO: Check if cookies are added to the CookieJar
     */
    fun followRedirect(redirectUrl: String) {
        val request = Request.Builder()
            .url(redirectUrl)
            .build()
        val response = okHttpClient.newCall(request).execute()
        if (!response.isSuccessful) throw RuntimeException("Failed to follow redirect")
    }

    /**
     * Sends a login request to the CAS, following auth flow
     * Cookies are added to the [okHttpClient] CookieJar and can be used for further requests
     *
     * @param casUrl Base URL of the CAS
     * @param redirectUrl URL the CAS will redirect us to
     * @param username Username
     * @param password Password
     * @param execution Execution key to use to login, if not provided, will be retrieved using [getLogin]
     */
    fun postLogin(
        casUrl: String,
        redirectUrl: String,
        casRequiresVerification: Boolean,
        username: String,
        password: String,
        execution: String? = null
    ) {
        var executionKey = execution
        if (execution == null) {
            try {
                getLogin(casUrl, redirectUrl)
            } catch (e: NoTicketException) {
                executionKey = e.executionKey
            }
        }
        val body =
            "username=${URLEncoder.encode(username, "UTF-8")}&password=${URLEncoder.encode(password, "UTF-8")}&execution=${URLEncoder.encode(executionKey, "UTF-8")}&_eventId=submit&geolocation=".toRequestBody(
                "application/x-www-form-urlencoded".toMediaTypeOrNull()
            )
        val request = Request.Builder()
            .url("$casUrl/login?service=$redirectUrl")
            .post(body)
            .build()
        val response = okHttpClient.newCall(request).execute()
        if (!response.isSuccessful) throw AuthException()
        if (casRequiresVerification && response.code == 200){
            if (!okHttpClient.cookieJar.loadForRequest(request.url).any {
                it.name.contains("TGC")
            }) {
                throw AuthException()
            }
        }
    }

    fun logout(casUrl: String) {
        val request = Request.Builder()
            .url("$casUrl/logout?service=")
            .build()
        val response = okHttpClient.newCall(request).execute()
        if (!response.isSuccessful) throw RuntimeException("Failed to logout")
    }
}