/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.universities.University

class StoredCredsRepository(context: Context) {
    private val masterKey: MasterKey
    private val encryptedSharedPreferences: SharedPreferences

    init {
        masterKey = MasterKey.Builder(context)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()
        encryptedSharedPreferences = EncryptedSharedPreferences.create(
            context,
            "scodocnotes_creds",
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    fun storeCreds(username: String, password: String, university: University) {
        val editor: SharedPreferences.Editor = encryptedSharedPreferences.edit()
        editor.putString("username", username)
        editor.putString("password", password)
        editor.putString("university", university.nom)
        editor.apply()
    }

    fun getCreds(): Triple<String, String, University>? {
        val username = encryptedSharedPreferences.getString("username", null)
        val password = encryptedSharedPreferences.getString("password", null)
        val university = encryptedSharedPreferences.getString("university", null)
        return if (username != null && password != null && university != null) {
            val univ = Graph.universities.find { it.nom == university }
            if (univ != null) Triple(username, password, univ) else null
        } else {
            null
        }
    }

    fun clearCreds() {
        val editor: SharedPreferences.Editor = encryptedSharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}