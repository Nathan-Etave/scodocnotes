/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import fr.julienrosse.scodocnotes.Graph
import java.io.IOException

class NewNoteNotificationWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {
    override suspend fun doWork(): Result {
        // TODO : Ouvrir l'appli si on clique sur la notif et l'enlever
        try {
            Graph.scodocRepository.updateDataPremiereConnexion()
        } catch (_: AuthException) {
            val notificationBuilder = NotificationCompat.Builder(applicationContext, "new_note")
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle("Problème d'identifiants")
                .setContentText("Impossible de se connecter à Scodoc Notes avec les identifiants enregistrés")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            with(NotificationManagerCompat.from(applicationContext)) {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.POST_NOTIFICATIONS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return Result.failure()
                }
                notify(789456123, notificationBuilder.build())
                return Result.failure()
            }
        } catch (_: IOException) {
            return Result.failure()
        } catch (_: DPCError) {
            return Result.failure()
        }
        if (!Graph.evaluationRepository.hasAnyKnownEvaluation()) {
            return Result.success()
        }
        val evaluations = Graph.evaluationRepository.getUnknownOrChangedEvaluations()
        if (evaluations.isNotEmpty()) {
            Graph.evaluationRepository.addEvaluations(evaluations)
            val notificationBuilder = NotificationCompat.Builder(applicationContext, "new_note")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Nouvelles notes")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            if (evaluations.size == 1){
                if (evaluations[0].ressource != null){
                    notificationBuilder.setContentText("Vous avez eu un ${evaluations[0].note.value} en ${evaluations[0].ressource!!.titre} !")
                }
                else {
                    notificationBuilder.setContentText("Vous avez eu un ${evaluations[0].note.value}!")
                }
            }
            else {
                notificationBuilder.setContentText("Vous avez eu ${evaluations.size} nouvelles notes !")
            }
            with(NotificationManagerCompat.from(applicationContext)) {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.POST_NOTIFICATIONS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return Result.failure()
                }
                notify(789456123, notificationBuilder.build())
                return Result.success()
            }
        }
        return Result.success()
    }
}