/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data.model

import com.google.gson.annotations.SerializedName

data class Releve(
    @SerializedName("formsemestre_id") val semestreId: Int,
    val ressources: Map<String, Ressource>,
    val saes: Map<String, SAE>,
    val ues: Map<String, UE>,
    val semestre: ReleveSemestre
) {
    companion object {
        fun populateMissing(releve: Releve) {
            releve.ressources.forEach { ressourceEntry ->
                val ressource = ressourceEntry.value
                ressource.code = ressourceEntry.key
                ressource.evaluations.forEach {
                    it.ressource = ressource
                }
            }
            releve.semestre.id = releve.semestreId
        }

        /**
         * Récupère toutes les évaluations du relevé
         * @param removeWithoutNotes si true, les évaluations sans note sont retirées
         * @return Liste des évaluations
         */
        fun getEvaluations(releve: Releve, removeWithoutNotes: Boolean = true): List<Evaluation> {
            val res = ArrayList<Evaluation>()
            val addedIds = HashSet<Int>()
            releve.ressources.forEach { ressourceEntry ->
                ressourceEntry.value.evaluations.forEach {
                    it.ressource = ressourceEntry.value
                    if (!removeWithoutNotes || it.note.value != null || it.id in addedIds) {
                        addedIds.add(it.id)
                        res.add(it)
                    }
                }
            }
            releve.saes.forEach { saeEntry ->
                saeEntry.value.evaluations.forEach {
                    if (!removeWithoutNotes || it.note.value != null || it.id in addedIds) {
                        addedIds.add(it.id)
                        res.add(it)
                    }
                }
            }
            return res
        }

        fun getUEs(releve: Releve): List<UE> {
            val res = ArrayList<UE>()
            releve.ues.forEach { ueEntry ->
                val ue = ueEntry.value
                ue.code = ueEntry.key
                res.add(ue)
            }
            return res
        }

        fun getRessources(releve: Releve): List<Ressource> {
            val res = ArrayList<Ressource>()
            releve.ressources.forEach { ressourceEntry ->
                val ressource = ressourceEntry.value
                ressource.code = ressourceEntry.key
                res.add(ressource)
            }
            return res
        }

        fun getSAEs(releve: Releve): List<SAE> {
            val res = ArrayList<SAE>()
            releve.saes.forEach { saeEntry ->
                val sae = saeEntry.value
                sae.code = saeEntry.key
                res.add(sae)
            }
            return res
        }

        fun calculMoyenne(evaluations: List<Evaluation>): Float? {
            var total = 0f
            var totalCoef = 0f
            evaluations.forEach {
                if (it.note.value != null) {
                    total += it.note.value * it.coef
                    totalCoef += it.coef
                }
            }
            val res = total / totalCoef
            return if (res.isNaN()) null else res
        }

        fun calculMoyenne(ressource: Ressource): Float? {
            return calculMoyenne(ressource.evaluations)
        }
    }
}
