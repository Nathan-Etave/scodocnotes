/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.universities.TestUniversity
import fr.julienrosse.scodocnotes.data.universities.University
import java.net.URLEncoder

class UserRepository(
    private val casFetcher: CASFetcher,
    private val storedCredsRepository: StoredCredsRepository
) {

    /**
     * Stores the provided credentials in the [StoredCredsRepository]
     * @param username Username
     * @param password Password
     * @see StoredCredsRepository
     */
    fun storeCreds(username: String, password: String) {
        storedCredsRepository.storeCreds(username, password, Graph.university)
    }

    /**
     * Retrieves the stored credentials from the [StoredCredsRepository]
     * @return A [Pair] containing the username and password, or null if no credentials are stored
     */
    private fun getCreds(): Pair<String, String>? {
        val creds = storedCredsRepository.getCreds()
        return if (creds != null) {
            Graph.university = creds.third
            Pair(creds.first, creds.second)
        } else null
    }

    /**
     * Returns true if creds are stored in the [StoredCredsRepository]
     * @return True if creds are stored, false otherwise
     */
    fun hasStoredCreds(): Boolean {
        return getCreds() != null
    }

    private fun buildRedirectUrl(university: University) : String {
        return university.scodocBaseUrl+"/services/doAuth.php?href="+URLEncoder.encode(university.scodocBaseUrl+"/", "UTF-8")
    }
    /**
     * Attempts to login to CAS using cookies in okhttp's cookie jar
     * If fails, attempts to login using stored credentials
     * @throws AuthException if login fails with stored credentials
     */
    fun login(username: String? = null, password: String? = null) {
        if (Graph.university is TestUniversity) {
            if (username == "googleTest" && password == "googleTest") {
                return
            } else {
                throw AuthException()
            }
        }
        try {
            val serviceUrl =
                casFetcher.getLogin(Graph.university.casBaseUrl, buildRedirectUrl(Graph.university))
            casFetcher.followRedirect(serviceUrl)
            // If we get here, login was successful

        } catch (e: NoTicketException) {
            if (username != null && password != null) {
                casFetcher.postLogin(
                    Graph.university.casBaseUrl,
                    buildRedirectUrl(Graph.university),
                    Graph.university.casRequiresVerification,
                    username,
                    password,
                    e.executionKey
                )
            } else {
                val creds = getCreds() ?: throw AuthException()
                casFetcher.postLogin(
                    Graph.university.casBaseUrl,
                    buildRedirectUrl(Graph.university),
                    Graph.university.casRequiresVerification,
                    creds.first,
                    creds.second,
                    e.executionKey
                )
            }

        }
    }
    fun logout() {
        storedCredsRepository.clearCreds()
        casFetcher.logout(Graph.university.casBaseUrl)
    }
}