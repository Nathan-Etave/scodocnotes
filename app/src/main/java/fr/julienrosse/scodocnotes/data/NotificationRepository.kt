/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context

class NotificationRepository(context: Context) {
    init {
        createNotificationChannel(context)
    }

    private fun createNotificationChannel(context: Context) {
        val name = "Nouvelles notes"
        val descriptionText = "Notification quand de nouvelles notes sont disponibles"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel("new_note", name, importance).apply {
            description = descriptionText
        }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }


}