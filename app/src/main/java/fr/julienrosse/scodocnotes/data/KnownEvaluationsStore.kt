/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import fr.julienrosse.scodocnotes.data.model.Evaluation
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map

class KnownEvaluationsStore(context: Context) {
    private val Context.knownEvalDataStore: DataStore<Preferences> by preferencesDataStore(name = "known_evaluations")
    private val dataStore = context.knownEvalDataStore


    suspend fun addEvaluation(evaluation: Evaluation) {
        dataStore.edit { preferences ->
            preferences[floatPreferencesKey(evaluation.id.toString())] =
                evaluation.note.value ?: -1f
        }
    }

    suspend fun getKnownEvaluations(): Map<Int, Float?> {
        val knownEvaluations = mutableMapOf<Int, Float?>()
        dataStore.data.map { preferences ->
            preferences.asMap().keys.forEach { key ->
                val value = preferences[key] as Float
                knownEvaluations[key.name.toInt()] = if (value == -1f) null else value
            }
        }.firstOrNull()
        return knownEvaluations
    }
}