/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.data

import fr.julienrosse.scodocnotes.data.model.Evaluation
import fr.julienrosse.scodocnotes.data.model.Releve

class EvaluationRepository(
    private val scodocRepository: ScodocRepository,
    private val knownEvaluationsStore: KnownEvaluationsStore
) {
    suspend fun hasAnyKnownEvaluation(): Boolean {
        return knownEvaluationsStore.getKnownEvaluations().isNotEmpty()
    }
    suspend fun getUnknownOrChangedEvaluations(): List<Evaluation> {
        val knownEvaluations = knownEvaluationsStore.getKnownEvaluations()
        val evaluations = Releve.getEvaluations(scodocRepository.dataPremiereConnexion!!.releve)
        return evaluations.filter { evaluation ->
            knownEvaluations[evaluation.id] == null || knownEvaluations[evaluation.id] != evaluation.note.value
        }
    }

    suspend fun addEvaluations(evaluations: List<Evaluation>) {
        evaluations.forEach { evaluation ->
            knownEvaluationsStore.addEvaluation(evaluation)
        }
    }
}