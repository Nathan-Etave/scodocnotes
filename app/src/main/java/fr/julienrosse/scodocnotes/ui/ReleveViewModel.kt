/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.DPCError
import fr.julienrosse.scodocnotes.data.ScodocRepository
import fr.julienrosse.scodocnotes.data.model.Releve
import fr.julienrosse.scodocnotes.data.model.ReleveSemestre
import fr.julienrosse.scodocnotes.data.model.Ressource
import fr.julienrosse.scodocnotes.data.model.SAE
import fr.julienrosse.scodocnotes.data.model.UE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class ReleveViewModel(
    private val scodocRepository: ScodocRepository = Graph.scodocRepository
) : ViewModel() {
    val uiState = mutableStateOf(ReleveState())

    init {
        update(false)
    }

    fun update(force: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            if (scodocRepository.dataPremiereConnexion == null || force) {
                withContext(Dispatchers.Main) {
                    uiState.value = uiState.value.copy(loading = true)
                }
            }
            try {
                if (scodocRepository.dataPremiereConnexion == null || force) scodocRepository.updateDataPremiereConnexion()
                withContext(Dispatchers.Main) {
                    val releve = scodocRepository.dataPremiereConnexion!!.releve
                    uiState.value = ReleveState(
                        loading = false,
                        error = null,
                        selectedIndex = uiState.value.selectedIndex,
                        semestre = releve.semestre,
                        ues = Releve.getUEs(releve),
                        ressources = Releve.getRessources(releve),
                        saes = Releve.getSAEs(releve)
                    )
                }
            } catch (e: IOException) {
                withContext(Dispatchers.Main) {
                    uiState.value = uiState.value.copy(loading = false, error = e)
                }
            } catch (e: DPCError) {
                withContext(Dispatchers.Main) {
                    uiState.value = uiState.value.copy(loading = false, error = e)
                }
            }
            //TODO : Handle AuthException by redirecting to login
        }
    }

    fun onSelectedIndexChange(index: Int) {
        uiState.value = uiState.value.copy(selectedIndex = index)
    }
}

data class ReleveState(
    val loading: Boolean = true,
    val error: Exception? = null,
    val selectedIndex: Int = 0,
    val semestre: ReleveSemestre? = null,
    val ues: List<UE> = emptyList(),
    val ressources: List<Ressource> = emptyList(),
    val saes: List<SAE> = emptyList()
)