/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import fr.julienrosse.scodocnotes.data.universities.University

@Composable
fun LoginScreen(
    loginViewModel: LoginViewModel
) {

    Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxSize()) {
        if (loginViewModel.lastAttemptFailedWrongCreds) {
            Toast.makeText(
                LocalContext.current,
                "Nom d'utilisateur / Mot de passe incorrect",
                Toast.LENGTH_LONG
            ).show()
            loginViewModel.acknowledgeLastAttemptFailed()
        }
        if (loginViewModel.networkError != "") {
            Toast.makeText(LocalContext.current, loginViewModel.networkError, Toast.LENGTH_LONG)
                .show()
            loginViewModel.acknowledgeNetworkError()
        }
        UniversitySelector(
            universities = loginViewModel.universities,
            selectedUniversity = loginViewModel.selectedUniversity,
            onSelectedUniversityChange = { loginViewModel.selectedUniversity = it }
        )
        LoginForm(
            username = loginViewModel.username,
            onUsernameChange = { loginViewModel.updateUsername(it) },
            password = loginViewModel.password,
            onPasswordChange = { loginViewModel.updatePassword(it) },
            enabled = loginViewModel.enabled
        )
        Button(
            onClick = { loginViewModel.submitForm() },
            content = { Text(text = "Se connecter") },
            enabled = loginViewModel.enabled && loginViewModel.submitButtonEnabled
        )
        if (!loginViewModel.enabled) {
            LoadingIndicator()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UniversitySelector(
    universities: List<University>,
    selectedUniversity: University,
    onSelectedUniversityChange: (University) -> Unit
) {

    var expanded by remember { mutableStateOf(false) }
    ExposedDropdownMenuBox(expanded = expanded, onExpandedChange = { expanded = !expanded }) {
        OutlinedTextField(
            value = selectedUniversity.nom,
            onValueChange = {},
            modifier = Modifier.menuAnchor(),
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
            readOnly = true,
            label = { Text(text = "Université") },
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded) },
        )
        ExposedDropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            universities.forEach { university ->
                UniversityDropdownItem(
                    university = university,
                    onSelectedUniversityChange = {
                        expanded = false;onSelectedUniversityChange(university)
                    })
            }
        }
    }
}

@Composable
fun UniversityDropdownItem(
    university: University,
    onSelectedUniversityChange: (University) -> Unit
) {
    DropdownMenuItem(
        text = { Text(text = university.nom) },
        onClick = { onSelectedUniversityChange(university) })
}

@Composable
private fun LoginForm(
    username: String,
    password: String,
    onUsernameChange: (String) -> Unit,
    onPasswordChange: (String) -> Unit,
    enabled: Boolean
) {
    val kbOptions = KeyboardOptions(
        KeyboardCapitalization.None,
        false,
        KeyboardType.Password,
        ImeAction.Default
    )
    Column {
        OutlinedTextField(
            value = username,
            onValueChange = onUsernameChange,
            label = { Text(text = "Nom d'utilisateur ENT") },
            enabled = enabled,
            keyboardOptions = kbOptions
        )
        OutlinedTextField(
            value = password,
            onValueChange = onPasswordChange,
            label = { Text(text = "Mot de passe ENT") },
            visualTransformation = PasswordVisualTransformation(),
            enabled = enabled,
            keyboardOptions = kbOptions
        )
    }
}