/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.AuthException
import fr.julienrosse.scodocnotes.data.universities.University
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class LoginViewModel(
    private val onLoginSuccess: () -> Unit,
    private val onLoginFailure: () -> Unit

    ) : ViewModel() {

    private val _username = mutableStateOf("")
    private val _password = mutableStateOf("")
    private val _enabled = mutableStateOf(true)
    private val _submitButtonEnabled = mutableStateOf(false)
    private val _lastAttemptWrongCreds = mutableStateOf(false)
    private val _universities = mutableStateOf(Graph.universities)
    private val _selectedUniversity = mutableStateOf(_universities.value[0])
    private val _networkError = mutableStateOf("")
    val username: String get() = _username.value
    val password: String get() = _password.value
    val enabled: Boolean get() = _enabled.value
    val submitButtonEnabled: Boolean get() = _submitButtonEnabled.value
    val lastAttemptFailedWrongCreds: Boolean get() = _lastAttemptWrongCreds.value
    val universities: List<University> get() = _universities.value
    var selectedUniversity: University
        get() = _selectedUniversity.value
        set(value) {
            _selectedUniversity.value = value
            Graph.university = value
        }
    val networkError: String get() = _networkError.value
    fun updateUsername(username: String) {
        _username.value = username
        _submitButtonEnabled.value = username.isNotEmpty() && password.isNotEmpty()
    }

    fun updatePassword(password: String) {
        _password.value = password
        _submitButtonEnabled.value = username.isNotEmpty() && password.isNotEmpty()
    }

    fun acknowledgeLastAttemptFailed() {
        _lastAttemptWrongCreds.value = false
    }

    fun acknowledgeNetworkError() {
        _networkError.value = ""
    }

    fun submitForm() {
        _enabled.value = false
        _networkError.value = ""
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    Graph.userRepository.login(username, password)
                    Graph.userRepository.storeCreds(username, password)
                    withContext(Dispatchers.Main) {
                        onLoginSuccess()
                    }
                } catch (e: AuthException) {
                    _enabled.value = true
                    _lastAttemptWrongCreds.value = true
                    withContext(Dispatchers.Main) {
                        onLoginFailure()
                    }
                } catch (e: IOException) {
                    _enabled.value = true
                    _networkError.value = e.message ?: "Erreur réseau"
                }
            }
        }
    }
}