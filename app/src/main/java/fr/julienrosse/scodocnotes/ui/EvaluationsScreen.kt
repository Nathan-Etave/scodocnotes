/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.em
import androidx.lifecycle.viewmodel.compose.viewModel
import fr.julienrosse.scodocnotes.data.model.Evaluation
import fr.julienrosse.scodocnotes.data.model.Note
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun EvaluationsScreen(
    evaluationsViewModel: EvaluationsViewModel = viewModel(),
) {
    val evaluations = evaluationsViewModel.uiState.value.evaluations
    Column {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(text = "Evaluations sans notes", color = MaterialTheme.colorScheme.primary)
            Spacer(modifier = Modifier.weight(1f))
            Switch(
                checked = evaluationsViewModel.uiState.value.showEvaluationsSansNote,
                onCheckedChange = { evaluationsViewModel.toggleEvaluationsSansNote() })
        }
        if (evaluationsViewModel.uiState.value.loading) {
            LoadingIndicator()
        }
        if (evaluationsViewModel.uiState.value.error != null) {
            NetworkErrorIndicator(e = evaluationsViewModel.uiState.value.error!!) {
                evaluationsViewModel.update()
            }
        }
        if (evaluations != null) {
            EvaluationsList(evaluations)
        }
    }

}

@Composable
fun EvaluationsList(evaluations: List<Evaluation>) {
    LazyColumn {
        items(evaluations.size) {
            EvaluationItem(evaluations[it])
        }
    }
}

@Composable
fun EvaluationItem(evaluation: Evaluation) {
    val dtf: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
    val date = evaluation.date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(dtf)
    androidx.compose.material3.ListItem(
        headlineContent = {
            Text(
                text = evaluation.description ?: "Aucune description",
                color = MaterialTheme.colorScheme.primary,
                fontStyle = (if (evaluation.description == null) FontStyle.Italic else FontStyle.Normal)
            )
        },
        supportingContent = {
            evaluation.ressource?.let {
                Text(
                    text = it.titre,
                    color = MaterialTheme.colorScheme.secondary
                )
            }
            Text(text = date)
            Text(text = "Coef : " + evaluation.coef.toString())
            Text(
                text = "Min / Max / Moy : " + Note.formatNote(evaluation.note.min) + " / " + Note.formatNote(
                    evaluation.note.max
                ) + " / " + Note.formatNote(evaluation.note.moy)
            )

        },
        trailingContent = {
            Text(text = Note.formatNote(evaluation.note.value), fontSize = 4.em)
        }
    )
}