/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.material.icons.filled.Functions
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Remove
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import fr.julienrosse.scodocnotes.data.model.Evaluation
import fr.julienrosse.scodocnotes.data.model.Note
import fr.julienrosse.scodocnotes.data.model.Releve
import fr.julienrosse.scodocnotes.data.model.ReleveSemestre
import fr.julienrosse.scodocnotes.data.model.Ressource
import fr.julienrosse.scodocnotes.data.model.SAE
import fr.julienrosse.scodocnotes.data.model.UE
import kotlinx.coroutines.launch
import kotlin.math.abs


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ReleveScreen(
    releveViewModel: ReleveViewModel = viewModel()
) {
    val state = releveViewModel.uiState.value
    val refreshScope = rememberCoroutineScope()
    var refreshing by remember { mutableStateOf(false) }
    fun refresh() = refreshScope.launch {
        refreshing = true
        releveViewModel.update(true)
        refreshing = false
    }
    val refreshState = rememberPullRefreshState(refreshing, ::refresh)
    Column(
        Modifier
            .background(MaterialTheme.colorScheme.surface)
            .pullRefresh(refreshState)
    ) {
        PullRefreshIndicator(refreshing, refreshState, Modifier.align(CenterHorizontally))
        SemestreOverview(state.semestre)
        ReleveTabs(
            selectedIndex = state.selectedIndex,
            onSelectedIndexChange = { i: Int -> releveViewModel.onSelectedIndexChange(i) })
        if (state.loading) {
            LoadingIndicator()
        }
        if (state.error != null) {
            NetworkErrorIndicator(state.error) {
                releveViewModel.update(true) // Forces the update as the state is probably incorrect at this point
            }
        }
        when (state.selectedIndex) {
            0 -> UEList(state.ues)
            1 -> RessourceList(state.ressources)
            2 -> SAEList(state.saes)
        }
    }
}

@Composable
fun SemestreOverview(semestre: ReleveSemestre?) {
    var showBottomSheet by remember { mutableStateOf(false) }
    if (showBottomSheet && semestre != null) {
        SemestreBottomSheet(semestre = semestre, onDismiss = { showBottomSheet = false })
    }
    Column(
        Modifier
            .padding(8.dp)
            .clickable { showBottomSheet = true }) {
        if (semestre != null) {
            Row {
                Text(
                    text = "Moyenne générale : ${Note.formatNote(semestre.moyenne.value)}",
                    color = MaterialTheme.colorScheme.onBackground
                )
                Spacer(modifier = Modifier.weight(1f))
                if (semestre.rang != null) {
                    Text(
                        text = "Rang : ${semestre.rang.rang}/${semestre.rang.total}",
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }
            }
            Row {
                if (semestre.absences.total > 0) {
                    Text(
                        text = "${semestre.absences.total} ${if (semestre.absences.total>1) "absences" else "absence"}, dont ${semestre.absences.injustifiees} ${if (semestre.absences.injustifiees>1) "injustifiées" else "injustifiée"}",
                        color = MaterialTheme.colorScheme.onBackground
                    )
                } else {
                    Text(text = "Aucune absence !", color = MaterialTheme.colorScheme.onBackground)
                }
            }
        } else {
            Text(text = "\n") // Pour garder la place
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SemestreBottomSheet(semestre: ReleveSemestre, onDismiss: () -> Unit) {
    ModalBottomSheet(onDismissRequest = onDismiss) {
        Column(horizontalAlignment = CenterHorizontally, modifier = Modifier.padding(16.dp)) {
            Text(text = "Moyenne générale :")
            Row {
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Person, contentDescription = "Ma note")
                    Text(
                        text = Note.formatNote(semestre.moyenne.value),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Add, contentDescription = "Note maximale")
                    Text(
                        text = Note.formatNote(semestre.moyenne.max),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Remove, contentDescription = "Note minimale")
                    Text(
                        text = Note.formatNote(semestre.moyenne.min),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Functions, contentDescription = "Moyenne de groupe")
                    Text(
                        text = Note.formatNote(semestre.moyenne.moy),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
            if (semestre.rang != null) {
                Spacer(modifier = Modifier.height(16.dp))

                Divider()
                Text(text = "Rang : ${semestre.rang.rang}/${semestre.rang.total}")
            }
            Spacer(modifier = Modifier.height(16.dp))
            Divider()
            if (semestre.absences.total > 0) {
                Text(
                    text = "${semestre.absences.total} absences, dont ${semestre.absences.injustifiees} injustifiées"
                )
            } else {
                Text(text = "Aucune absence !")
            }
            Spacer(modifier = Modifier.height(32.dp))

        }
    }
}
@Composable
fun ReleveTabs(
    selectedIndex: Int,
    onSelectedIndexChange: (Int) -> Unit,
) {
    val titles = listOf("UEs", "Ressources", "SAEs")
    TabRow(
        selectedTabIndex = selectedIndex,
        modifier = Modifier.zIndex(-1f)
    ) {
        titles.forEachIndexed { index, title ->
            Tab(
                selected = (selectedIndex == index),
                onClick = { onSelectedIndexChange(index) },
                text = { Text(text = title) }
            )
        }
    }
}

@Composable
fun UEList(
    ueList: List<UE> = emptyList()
) {
    LazyColumn(
        Modifier
            .fillMaxSize()
            .zIndex(-1f)
    ) {
        items(ueList) {
            UEListItem(it)
        }
    }
}

@Composable
fun RessourceList(ressources: List<Ressource>) {
    LazyColumn(
        Modifier
            .fillMaxSize()
            .zIndex(-1f)
    ) {
        items(ressources) {
            RessourceListItem(it)
        }
    }
}

@Composable
fun SAEList(
    saeList: List<SAE>
) {
    LazyColumn(
        Modifier
            .fillMaxSize()
            .zIndex(-1f)
    ) {
        items(saeList) {
            SAEListItem(it)
        }
    }
}

@Composable
fun UEListItem(ue: UE) {
    ListItem(
        headlineContent = { Text(text = "${ue.code} - ${ue.titre}") },
        trailingContent = {
            Text(
                text = Note.formatNote(ue.moyenne.value),
                style = MaterialTheme.typography.labelLarge
            )
        }
    )
}

@Composable
fun RessourceListItem(ressource: Ressource) {
    var isFolded by remember { mutableStateOf(true) }
    var showBottomSheet: Int? by remember { mutableStateOf(null) }
    if (showBottomSheet != null) {
        ressource.evaluations.find { it.id == showBottomSheet }
            ?.let { EvaluationBottomSheet(it) { showBottomSheet = null } }
    }
    Column {
        ListItem(
            headlineContent = { Text(text = ressource.titre) },
            leadingContent = {
                Icon(
                    if (isFolded) Icons.Default.ExpandMore else Icons.Default.ExpandLess,
                    contentDescription = if (isFolded) "Expand" else "Fold"
                )
            },
            trailingContent = {
                Text(
                    text = Note.formatNote(Releve.calculMoyenne(ressource)),
                    style = MaterialTheme.typography.labelLarge
                )
            },
            modifier = Modifier.clickable { isFolded = !isFolded }
        )
        if (!isFolded) {
            ressource.evaluations.forEach {
                ListItem(
                    headlineContent = {
                        if (it.description != null) {
                            Text(text = it.description)
                        }
                        else {
                            Text("Aucune description", fontStyle = FontStyle.Italic)
                        }
                    },
                    trailingContent = {
                        Text(
                            text = Note.formatNote(it.note.value),
                            style = MaterialTheme.typography.labelLarge
                        )
                    },
                    modifier = Modifier.clickable {
                        showBottomSheet = it.id
                    }
                )
            }
        }
    }

}

@Composable
fun SAEListItem(sae: SAE) {
    var isFolded by remember { mutableStateOf(true) }

    Column {
        ListItem(
            headlineContent = { Text(text = "${sae.code} - ${sae.titre}") },
            leadingContent = {
                Icon(
                    if (isFolded) Icons.Default.ExpandMore else Icons.Default.ExpandLess,
                    contentDescription = if (isFolded) "Expand" else "Fold"
                )
            },
            modifier = Modifier.clickable { isFolded = !isFolded }
        )
        if (!isFolded) {
            sae.evaluations.forEach {
                EvaluationItem(it)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
        /**
         * Note: Ne pas utiliser pour les evaluations de SAE
         */
fun EvaluationBottomSheet(evaluation: Evaluation, onDismiss: () -> Unit) {
    ModalBottomSheet(onDismissRequest = { onDismiss() }) {
        Column(
            horizontalAlignment = CenterHorizontally,
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = evaluation.description ?: "Aucune description",
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(16.dp),
                fontStyle = (if (evaluation.description == null) FontStyle.Italic else FontStyle.Normal)
            )
            Text(
                text = "${evaluation.ressource!!.code} - ${evaluation.ressource!!.titre}",
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(12.dp)
            )
            Divider(Modifier.padding(12.dp))
            Spacer(modifier = Modifier.height(12.dp))
            Row {
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Person, contentDescription = "Ma note")
                    Text(
                        text = Note.formatNote(evaluation.note.value),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Add, contentDescription = "Note maximale")
                    Text(
                        text = Note.formatNote(evaluation.note.max),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Remove, contentDescription = "Note minimale")
                    Text(
                        text = Note.formatNote(evaluation.note.min),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
                Row(Modifier.weight(1f)) {
                    Icon(Icons.Default.Functions, contentDescription = "Moyenne de groupe")
                    Text(
                        text = Note.formatNote(evaluation.note.moy),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
            Spacer(modifier = Modifier.height(12.dp))
            Row {
                Text("Coefficient")
                Spacer(modifier = Modifier.weight(1f))
                Text(text = evaluation.coef.toString())
            }
            Spacer(modifier = Modifier.height(12.dp))
            val varRes = Evaluation.varRessource(evaluation)
            if (varRes != null) {
                Row {
                    Text("Variation moyenne de ressource")
                    Spacer(modifier = Modifier.weight(1f))
                    Text(text = (if (varRes > 0f) "+" else "-") + Note.formatNote(abs(varRes)))
                }
            }
        }
    }
}