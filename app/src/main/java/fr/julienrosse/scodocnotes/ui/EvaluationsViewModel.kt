/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.julienrosse.scodocnotes.Graph
import fr.julienrosse.scodocnotes.data.DPCError
import fr.julienrosse.scodocnotes.data.ScodocRepository
import fr.julienrosse.scodocnotes.data.model.Evaluation
import fr.julienrosse.scodocnotes.data.model.Releve
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class EvaluationsViewModel(
    private val scodocRepository: ScodocRepository = Graph.scodocRepository
) : ViewModel() {
    val uiState = mutableStateOf(EvaluationsState())

    init {
        update()
    }
    fun update(){
        if (scodocRepository.dataPremiereConnexion == null){
            uiState.value = uiState.value.copy(loading = true)
            viewModelScope.launch(Dispatchers.IO){
                try{
                    scodocRepository.updateDataPremiereConnexion()
                    withContext(Dispatchers.Main) {
                        update()
                    }
                } catch (e: IOException){
                    withContext(Dispatchers.Main) {
                        uiState.value = uiState.value.copy(error = e, loading = false)
                    }
                } catch (e: DPCError){
                    withContext(Dispatchers.Main) {
                        uiState.value = uiState.value.copy(error = e, loading = false)
                    }
                }
            }
        }
        else{
            uiState.value = uiState.value.copy(
                releve = scodocRepository.dataPremiereConnexion!!.releve,
                evaluations = scodocRepository.dataPremiereConnexion!!.releve.let { relev ->
                    Releve.getEvaluations(
                        relev, !uiState.value.showEvaluationsSansNote
                    ).sortedByDescending { it.date }
                },
                loading = false,
                error = null
            )
        }
    }
    fun toggleEvaluationsSansNote() {
        uiState.value =
            uiState.value.copy(showEvaluationsSansNote = !uiState.value.showEvaluationsSansNote)
        uiState.value = uiState.value.copy(evaluations = uiState.value.releve?.let { releve ->
            Releve.getEvaluations(
                releve, !uiState.value.showEvaluationsSansNote
            ).sortedByDescending { it.date }
        })

    }
}

data class EvaluationsState(
    val loading: Boolean = false,
    val error: Exception? = null,
    val releve: Releve? = null,
    val evaluations: List<Evaluation>? = null,
    val showEvaluationsSansNote: Boolean = false
)