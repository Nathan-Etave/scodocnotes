/*
 *     Copyright (C) 2023  Julien ROSSE
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.julienrosse.scodocnotes

import okhttp3.Interceptor
import okhttp3.Response

class UserAgentAdderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newRequest = request.newBuilder()
            .removeHeader("User-Agent")
            .addHeader(
                "User-Agent",
                "ScodocNotes-Android/${BuildConfig.VERSION_NAME} par julien.rosse(at)etu.univ-orleans.fr"
            )
            .build()
        return chain.proceed(newRequest)
    }
}